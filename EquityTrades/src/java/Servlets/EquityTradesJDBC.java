package Servlets;

import java.sql.*;

public class EquityTradesJDBC {
    public static String databaseURL = "jdbc:postgresql://130.245.72.78:5432/cse305";
    public static String user = "postgres";
    public static String dbPassword = "";
    

    public static void executeOnDB(String queryString){
        Connection connection = null;
        Statement statement = null;
        try{
            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(databaseURL,user,dbPassword);

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            statement = connection.createStatement();
            String sql = queryString;
            ResultSet rs = statement.executeQuery(sql);

            //STEP 5: Extract data from result set
            while(rs.next()){
                //Retrieve by column name
                
                /*int ssn  = rs.getInt("ssn");
                String lastName  = rs.getString("lastname");
                String firstName  = rs.getString("firstname");
                String address  = rs.getString("address");
                int zipCode = rs.getInt("zipcode");
                long telephone = rs.getLong("telephone");

                //Display values
                System.out.println("SSN: " + ssn);
                System.out.println("lastName: " + lastName);
                System.out.println("firstName: " + firstName);
                System.out.println("address: " + address);
                System.out.println("zipCode: " + zipCode);
                System.out.println("telephone: \n" + telephone);*/
            } 
            //STEP 6: Clean-up environment
            rs.close();
            statement.close();
            connection.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
      
        }//end try
        System.out.println("Goodbye!");
        
    }//end 
    
    
    public static void displayPersonTable(){
        Connection connection = null;
        Statement statement = null;
        try{
            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,user,dbPassword);

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            statement = connection.createStatement();
            String sql = "SELECT * FROM PERSON";
            ResultSet rs = statement.executeQuery(sql);

            //STEP 5: Extract data from result set
            while(rs.next()){
                //Retrieve by column name
                
                int ssn  = rs.getInt("ssn");
                String lastName  = rs.getString("lastname");
                String firstName  = rs.getString("firstname");
                String address  = rs.getString("address");
                int zipCode = rs.getInt("zipcode");
                long telephone = rs.getLong("telephone");

                //Display values
                System.out.println("SSN: " + ssn+"\tlastName: "
                        + lastName+"\tfirstName: " + firstName+"\taddress: "
                        + address+"\tzipCode: " + zipCode+"\ttelephone: " + telephone);
            } 
            //STEP 6: Clean-up environment
            rs.close();
            statement.close();
            connection.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
      
        }//end try
        System.out.println("Finished Displaying Persons Table.");
    }//end 
    
    public static void displayStockTable(){
        Connection connection = null;
        Statement statement = null;
        try{
            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            connection = DriverManager.getConnection(databaseURL,user,dbPassword);

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            statement = connection.createStatement();
            String sql = "SELECT * FROM Stock";
            ResultSet rs = statement.executeQuery(sql);

            //STEP 5: Extract data from result set
            while(rs.next()){
                //Retrieve by column name
                
                String stockSymbol  = rs.getString("stocksymbol");
                String companyName  = rs.getString("companyname");
                String type  = rs.getString("type");
                double pricePerShare  = rs.getDouble("pricepershare");

                //Display values
                System.out.println("stockSymbol: " + stockSymbol 
                        + "\tcompanyName: " + companyName+"\ttype: " 
                        + type+"\tpricePerShare: " + pricePerShare);
            }
                
            //STEP 6: Clean-up environment
            rs.close();
            statement.close();
            connection.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
      
        }//end try
        System.out.println("Finished Displaying Persons Table.");
    }//end 
}

        
