package Servlets;

import Beans.StockSearchBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static sun.util.logging.LoggingSupport.log;

@WebServlet (name="StockSuggestionsForCustomerServlet",urlPatterns={"/StockSuggestionsForCustomerServlet"}) 
public class StockSuggestionsForCustomerServlet extends HttpServlet {

   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
       throws ServletException, IOException {
         
       // CONNECT AND SQL QUERIES 
        Connection connection = null;
        Statement statement = null;
        String nodeString = "Suggestions Created For client ";
        try{
            String clientIdParam = request.getParameter("clientIdParam");
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,EquityTradesJDBC.user,EquityTradesJDBC.dbPassword);
            statement = connection.createStatement();
 
            String queryString =  "DROP VIEW IF EXISTS StockSuggestionsForClient" +clientIdParam
                    + "; CREATE VIEW StockSuggestionsForClient" +clientIdParam +
                    " AS SELECT S.StockSymbol AS symbol, S.companyName AS company " + 
                    " FROM Stock S WHERE S.Type IN ( SELECT S1.Type FROM Stock S1, Trade T " +
              " WHERE S1.StockSymbol = T.StockId AND T.AccountId IN ( SELECT A.Id FROM Account A WHERE A.ClientId =" + clientIdParam 
                +") GROUP BY S1.Type ORDER BY COUNT(S1.Type) DESC limit 1);";
            ResultSet rs = statement.executeQuery(queryString);
            //PARSE THE RESULTS SET 
            /*String nodeString=  "<table style=\"width:100%\"> ";
            while(rs.next()){
                //Retrieve DATA by column name
                String stock = rs.getString("stocksymbol");
                    nodeString = nodeString + 
                                "  <tr> " +
                                "    <td>" + stock + " </td> " +
                                "  </tr> ";
            }
            nodeString += "</table>";*/
            //STEP 6: Clean-up environment
            nodeString = "Suggestions Created For client " + clientIdParam;
           
            //CLEAN UP CONNECTION
            rs.close();
            statement.close();
            connection.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (ClassNotFoundException ex) {
           Logger.getLogger(StockSuggestionsForCustomerServlet.class.getName()).log(Level.SEVERE, null, ex);
       }finally{
            //finally block used to close resources
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
      
        }
         request.setAttribute("stocksuggestions", nodeString);
         String url = "./RStockSuggestionOutputJSP.jsp";
         RequestDispatcher dispatcher = request.getRequestDispatcher(url);
         dispatcher.forward(request, response);
    }
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    public String getServletInfo() {
        return "description";
    }
    
}
