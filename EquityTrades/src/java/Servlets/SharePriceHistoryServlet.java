package Servlets;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet (name="SharePriceHistoryServlet",urlPatterns={"/SharePriceHistoryServlet"}) 
public class SharePriceHistoryServlet extends HttpServlet {

   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
       throws ServletException, IOException {
       
       String StockSymbolParam = request.getParameter("StockSymbolParam");
       String StartDateParam = request.getParameter("StartDateParam");
       String[] startTokens;
       startTokens = StartDateParam.split("/-");
       String StartDateStr="";
       for(int i=0;i<startTokens.length;i++){
           StartDateStr += startTokens[i];
       }
       String EndDateParam = request.getParameter("EndDateParam");
       String[] endTokens;
       endTokens = EndDateParam.split("/-");
       String EndDateStr="";
       for(int i=0;i<endTokens.length;i++){
           EndDateStr += endTokens[i];
       }
       String nodeString = "      Error finding share price history.";
       // CONNECT AND SQL QUERIES 
        Connection connection = null;
        Statement statement = null;
        try{
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,EquityTradesJDBC.user,EquityTradesJDBC.dbPassword);
            statement = connection.createStatement();
            String queryString = "SELECT R.DateTime AS Date, AVG(R.PricePerShare::numeric) AS AvgPrice FROM Transactions R,Trade T, Stock S " +
            " WHERE T.TransactionId = R.Id AND T.StockId = S.StockSymbol AND StockSymbol = "
                    + "'" + StockSymbolParam +
             "' AND R.DateTime >= '" + StartDateStr+ "'" + 
            " AND R.DateTime <= '" + EndDateStr+ "'" +
            " GROUP BY R.DateTime ORDER BY R.DateTime ASC;";
            
            ResultSet rs = statement.executeQuery(queryString);
            nodeString="<table style=\"width:100%\"> ";
            //PARSE THE RESULTS SET 
            while(rs.next()){
                //Retrieve DATA by column name
                String date  = rs.getString("Date");
                double avgPrice  = rs.getDouble("AvgPrice");

                //BUILD NODE STRING FOR JSP
                 nodeString = nodeString + 
                                "  <tr> " +
                                "    <td>" + date + " </td> " +
                                "    <td>" + "$"+ avgPrice + " </td> " +
                                "  </tr> ";
                
            }
             nodeString += "</table>";
            
            //SEND REQUEST TO JSP 
             
            
            //CLEAN UP CONNECTION
            rs.close();
            statement.close();
            connection.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (ClassNotFoundException ex) {
           Logger.getLogger(SharePriceHistoryServlet.class.getName()).log(Level.SEVERE, null, ex);
       }finally{
            //finally block used to close resources
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
      
        }
        request.setAttribute("sharePriceHistoryOverTime", nodeString);
        String url ="./C_SharePriceHistoryJSP.jsp" ;
        RequestDispatcher dispatcher = request.getRequestDispatcher(url);
            dispatcher.forward(request,response);  
    }
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    public String getServletInfo() {
        return "description";
    }
    
}
