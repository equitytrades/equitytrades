/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Wilson
 */
@WebServlet(name = "SearchOrdersServlet", urlPatterns = {"/SearchOrdersServlet"})
public class SearchOrdersServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
       throws ServletException, IOException {
       String choice = request.getParameter("choice");
       String input = request.getParameter("input");
       int clientid = (int)request.getSession().getAttribute("clientid");
       
       // CONNECT AND SQL QUERIES 
        Connection connection = null;
        Statement statement = null;
        try{
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,EquityTradesJDBC.user,EquityTradesJDBC.dbPassword);
            statement = connection.createStatement();
            int accountid = 0;
            String queryString = "SELECT id FROM account WHERE clientid = " + clientid + ";";
            ResultSet rs = statement.executeQuery(queryString);
            while(rs.next()){
                accountid = rs.getInt("id");
            }
            queryString = "SELECT Orders.Id, Orders.DateTime,Orders.PricePerShare,Orders.Price,Orders.Orders " +
            "FROM Trade INNER JOIN Orders ON Trade.OrderID = Orders.ID "+
            "WHERE Trade.AccountID = " + accountid + " AND Trade.StockId IN " + 
            "(SELECT StockSymbol FROM Stock WHERE type = '" + input + "' ) ORDER BY datetime DESC; ";
            if (choice.equals("keyword")){
                queryString = "SELECT Orders.Id, Orders.DateTime,Orders.PricePerShare,Orders.Price,Orders.Orders " +
                    "FROM Trade INNER JOIN Orders ON Trade.OrderID = Orders.ID "+
                    "WHERE Trade.StockId IN " + 
                    "(SELECT StockSymbol FROM Stock WHERE companyname LIKE '%" + input + "%') ORDER BY datetime DESC; ";
            }
            rs = statement.executeQuery(queryString);
            String nodeString=  "<table style=\"width:100%\"><tr><td>Order ID</td><td>Date</td><td>Price per Share</td><td>Price Type</td><td>Order Type</td></tr>";
            //PARSE THE RESULTS SET 
            while(rs.next()){
                int id = rs.getInt("id");
                Date date = rs.getDate("datetime");
                String pricepershare = rs.getString("pricepershare");
                String price = rs.getString("price");
                String order = rs.getString("orders");
                nodeString = nodeString + 
                                "<tr>" +
                                "<td>" + id + "</td>" +
                                "<td>" + date + "</td>" +
                                "<td>" + pricepershare + "</td>" +
                                "<td>" + price + "</td>" +
                                "<td>" + order + "</td>" +
                                "</tr> ";
            }
            nodeString += "</table>";
            if (nodeString.equals("<table style=\"width:100%\"><tr><td>Order ID</td><td>Date</td><td>Price per Share</td><td>Price Type</td><td>Order Type</td></tr></table>")){
                nodeString = "       No orders found.";
            }
            request.setAttribute("search", nodeString);
            String url = "./C_SearchOrderOutputJSP.jsp";
            RequestDispatcher dispatcher = request.getRequestDispatcher(url);
            dispatcher.forward(request, response);
            //CLEAN UP CONNECTION
            rs.close();
            statement.close();
            connection.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (ClassNotFoundException ex) {
       }finally{
            //finally block used to close resources
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }  
    }
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    public String getServletInfo() {
        return "description";
    }
    
}
