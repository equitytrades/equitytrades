/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Beans.ClientBean;
import Beans.ClientList;
import Beans.EmployeeBean;
import Beans.EmployeeList;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Wilson
 */
@WebServlet (name="LoginServlet",urlPatterns={"/LoginServlet"})
public class LoginServlet extends HttpServlet {
    private Object file;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
            Connection connection = null;
            Statement statement = null;
            try{
            //STEP 3: Open a connection
                String username = request.getParameter("username");
                String password = request.getParameter("password");
                System.out.println("Connecting to database...");
                Class.forName("org.postgresql.Driver");
                int id = 0;
                connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,EquityTradesJDBC.user,EquityTradesJDBC.dbPassword);

            //STEP 4: Execute a query
                System.out.println("Creating statement...");
                statement = connection.createStatement();
                String sql = "SELECT * FROM login";
                String url = "./invalidclientJSP.jsp";
                ResultSet rs = statement.executeQuery(sql);

            //STEP 5: Extract data from result set
                while(rs.next()){
                    if (username.equals(rs.getString("username"))){
                        if (password.equals(rs.getString("password"))){
                            request.getSession().setAttribute("username", username);
                            id = rs.getInt("id");
                        }
                    }
                }
            if (id != 0){
                sql = "SELECT * FROM employee WHERE " + id + "= ssn;";
                rs = statement.executeQuery(sql);
                int hourlyrate = 0;
                int brokerId = 0;
                while (rs.next()){
                    hourlyrate = rs.getInt("hourlyrate");
                    brokerId = rs.getInt("id");
                }
                System.out.println("hourlyrate: " + hourlyrate);
                //no hourlyrate found, person is a client
                if (hourlyrate == 0){
                    System.out.println("person is a client");
                    url = "./ClientJSP.jsp";
                    request.getSession().setAttribute("user", "client");
                    request.getSession().setAttribute("clientid", id);
                }
                else {
                    System.out.println("person is an employee, load clients");
                    sql = "SELECT id, lastname, firstname, address, zipcode, telephone, email, rating, "
                            + "creditcardnumber  FROM person, client WHERE person.ssn = client.id;";
                    rs = statement.executeQuery(sql);
                    //initialize client benas and list
                    ClientList clientList = new ClientList();
                    request.getSession().setAttribute("cli", clientList);
                    while(rs.next()){
                        ClientBean client = new ClientBean();
                        client.setClientId(rs.getInt("id"));
                        client.setLastname(rs.getString("lastname"));
                        client.setFirstname(rs.getString("firstname"));
                        client.setAddress(rs.getString("address"));
                        client.setZipcode(rs.getInt("zipcode"));
                        client.setTelephone(rs.getLong("telephone"));
                        client.setEmail(rs.getString("email"));
                        client.setRating(rs.getInt("rating"));
                        client.setCreditcardnumber(rs.getLong("creditcardnumber"));
                        clientList.addClient(client);
                    }
                    //person logging in is a manager
                    if (hourlyrate > 99){
                        System.out.println("person is a manager, load employees");
                        sql = "SELECT * FROM employee, person WHERE employee.ssn = person.ssn;";
                        rs = statement.executeQuery(sql);
                    //initalize employee beans and list
                        EmployeeList employeeList = new EmployeeList();
                        request.getSession().setAttribute("emp", employeeList);
                        while(rs.next()){
                            EmployeeBean employee = new EmployeeBean();
                            employee.setEmployeeId(rs.getInt("id"));
                            employee.setSsn(rs.getInt("ssn"));
                            employee.setFirstname(rs.getString("firstname"));
                            employee.setLastname(rs.getString("lastname"));
                            employee.setAddress(rs.getString("address"));
                            employee.setZipcode(rs.getInt("zipcode"));
                            employee.setTelephone(rs.getLong("telephone"));
                            employee.setStartdate(rs.getDate("startdate"));
                            employee.setHourlyrate(rs.getInt("hourlyrate"));
                            employeeList.addEmployee(employee);
                        }
                        url = "./managerJSP.jsp";
                        request.getSession().setAttribute("user", "manager");
                    } 
                    else {
                        System.out.println("person is a representative");
                        url = "./RepresentativeJSP.jsp";
                        request.getSession().setAttribute("user", "representative");
                        request.getSession().setAttribute("brokerid", brokerId);
                    }  
                }
            }
                System.out.println(url);
                RequestDispatcher dispatcher = request.getRequestDispatcher(url);
                System.out.println(dispatcher);
                dispatcher.forward(request, response);
                 
            //STEP 6: Clean-up environment
            rs.close();
            statement.close();
            connection.close();
            }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
            }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
            }finally{
            //finally block used to close resources
                try{
                    if(statement!=null)
                        statement.close();
                }catch(SQLException se2){
                }// nothing we can do
                try{
                    if(connection!=null)
                        connection.close();
                }catch(SQLException se){
                    se.printStackTrace();
                }//end finally try
            }//end try
            System.out.println("Goodbye!");
        }//end 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
