/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Beans.EmployeeBean;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Wilson
 */
@WebServlet (name="EditEmployeeServlet",urlPatterns={"/EditEmployeeServlet"})
public class EditEmployeeServlet extends HttpServlet {
    static String databaseURL = "jdbc:postgresql://130.245.72.54:5432/cse305";
    static String user = "postgres";
    static String dbPassword = "";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Connection connection = null;
        Statement statement = null;
        String url = "./EditEmployeeFailJSP.jsp";
        String username = request.getParameter("username");//notnull
        String password = request.getParameter("password");//notnull
        String ssn = request.getParameter("ssn");//notnull\
        String firstname = request.getParameter("firstname");//notnull
        String lastname = request.getParameter("lastname");//notnull
        String address = request.getParameter("address");
        String zipcode = request.getParameter("zipcode");
        String telephone = request.getParameter("telephone");
        String startdate = request.getParameter("startdate"); //2015-11-11
        String hourlyrate = request.getParameter("hourlyrate");//notnull
        
        if (!ssn.equals("") && !firstname.equals("") && !lastname.equals("") && !hourlyrate.equals("")){
            System.out.println("inside if statement");
            if (address.equals(""))
                address = "NULL";
            else
                address = "'" + address + "'";
            if (zipcode.equals(""))
                zipcode = "NULL";
            if (telephone.equals(""))
                telephone = "NULL";
            if (startdate.equals(""))
                startdate = "NULL";
            else 
                startdate = "'" + startdate + "'";
            try{
                System.out.println("in try");
                System.out.println("Connecting to database...");
                Class.forName("org.postgresql.Driver");
                Class.forName("org.postgresql.Driver");
                connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,user,dbPassword);
                EmployeeBean employee = (EmployeeBean)request.getSession().getAttribute("empEdit");
                int empSsn = employee.getSsn();
                
                    
            //STEP 4: Execute a query
                System.out.println("Creating statement...");
                System.out.println("Address is " + address);
                statement = connection.createStatement();
                String sql = "UPDATE person SET ssn=" + ssn + ", lastname='" + lastname
                                + "', firstname='" + firstname + "', address=" + address 
                                + ", zipcode=" + zipcode + ", telephone=" + telephone + " WHERE ssn=" + empSsn + ";";
                statement.executeUpdate(sql);
                System.out.println("username is " + username);
                if (!username.equals("")){
                    System.out.println("username is " + username);
                    sql = " UPDATE login SET username='" + username + "' WHERE id =" + ssn + ";"; 
                    statement.executeUpdate(sql);
                }
                if (!password.equals("")){
                    sql = " UPDATE login SET password='" + password + "' WHERE id =" + ssn + ";"; 
                    statement.executeUpdate(sql);
                }
                sql = "update employee set startdate=" + startdate + ", hourlyrate=" + hourlyrate + " WHERE ssn=" + ssn + ";";
                statement.executeUpdate(sql);
                sql = "SELECT * FROM employee, person WHERE person.ssn = " + ssn + " AND employee.ssn = person.ssn;";
                ResultSet rs = statement.executeQuery(sql);
                System.out.println("reached rs");
                if (rs.next()){
                    employee.setEmployeeId(rs.getInt("id"));
                    employee.setSsn(rs.getInt("ssn"));
                    employee.setFirstname(rs.getString("firstname"));
                    employee.setLastname(rs.getString("lastname"));
                    employee.setAddress(rs.getString("address"));
                    employee.setZipcode(rs.getInt("zipcode"));
                    employee.setTelephone(rs.getLong("telephone"));
                    employee.setStartdate(rs.getDate("startdate"));
                    employee.setHourlyrate(rs.getInt("hourlyrate"));
                }
                url = "./EditEmployeeSuccessJSP.jsp";
            //STEP 6: Clean-up environment
                rs.close();
                statement.close();
                connection.close();
            }catch(SQLException se){
            //Handle errors for JDBC
                se.printStackTrace();
            }catch(Exception e){
            //Handle errors for Class.forName
                e.printStackTrace();
            }finally{
            //finally block used to close resources
                try{
                    if(statement!=null)
                        statement.close();
                }catch(SQLException se2){
                }// nothing we can do
                try{
                    if(connection!=null)
                       connection.close();
                }catch(SQLException se){
                    se.printStackTrace();
                }//end finally try
            }//end try
            System.out.println("Goodbye!");
        }//end 
        RequestDispatcher dispatcher = request.getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
