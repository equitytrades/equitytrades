package Servlets;

import Beans.StockSearchBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static sun.util.logging.LoggingSupport.log;

@WebServlet (name="SalesByStockTypeServlet",urlPatterns={"/SalesByStockTypeServlet"}) 
public class SalesByStockTypeServlet extends HttpServlet {

   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
       throws ServletException, IOException {
    
       // SET BEAN DATA
       String stockTypeField = (request.getParameter("stockTypeField"));
       if(stockTypeField!=null)stockTypeField = stockTypeField.toLowerCase();
       
       // CONNECT AND SQL QUERIES 
        Connection connection = null;
        Statement statement = null;
        try{
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,EquityTradesJDBC.user,EquityTradesJDBC.dbPassword);
            statement = connection.createStatement();
            String queryString = "SELECT SUM(R.Fee) AS Fees FROM Transactions R, Trade T, Stock S WHERE T.TransactionId = R.Id AND T.StockId = " + 
            " S.StockSymbol And S.Type= '"+ stockTypeField +"' GROUP BY S.Type ;";
            ResultSet rs = statement.executeQuery(queryString);
            
            String nodeString="<table style=\"width:100%\"> ";
            //PARSE THE RESULTS SET 
            while(rs.next()){
                //Retrieve DATA by column name
                double stockRev  = rs.getDouble("Fees");
                //BUILD NODE STRING FOR JSP
               nodeString = nodeString + 
                                "  <tr> " +
                                "    <td> " + stockTypeField + "</td> " +
                                "    <td> " + stockRev + "</td> " +
                                "  </tr> ";
                             
            }
            
            nodeString+="</table>";
            String storageKey = "StockRevByType";
            request.setAttribute(storageKey, nodeString);
            
            
            //SEND REQUEST TO JSP 
            String url ="./SalesByStockTypeJSP.jsp" ;
            RequestDispatcher dispatcher = request.getRequestDispatcher(url);
            dispatcher.forward(request,response);         
            
            //CLEAN UP CONNECTION
            rs.close();
            statement.close();
            connection.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (ClassNotFoundException ex) {
           Logger.getLogger(SalesByStockTypeServlet.class.getName()).log(Level.SEVERE, null, ex);
       }finally{
            //finally block used to close resources
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
      
        } 
    }
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    public String getServletInfo() {
        return "description";
    }
    
}
