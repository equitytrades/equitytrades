package Servlets;

import Beans.StockSearchBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static sun.util.logging.LoggingSupport.log;

@WebServlet (name="RevenueGeneratedByStockServlet",urlPatterns={"/RevenueGeneratedByStockServlet"}) 
public class RevenueGeneratedByStockServlet extends HttpServlet {

   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
       throws ServletException, IOException {
       
       String stockIdParam = request.getParameter("stockIdParam");
       if(stockIdParam!=null) stockIdParam=stockIdParam.toUpperCase();
       // CONNECT AND SQL QUERIES 
        Connection connection = null;
        Statement statement = null;
        try{
            System.out.println("Connecting to database...");
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,EquityTradesJDBC.user,EquityTradesJDBC.dbPassword);

            System.out.println("Creating statement...");
            statement = connection.createStatement();
            String queryString = "SELECT SUM(R.Fee) AS Rev FROM Transactions R, Trade T WHERE T.TransactionId = R.Id AND T.StockId = '"
                    + stockIdParam+"' GROUP BY T.StockId;";
            ResultSet rs = statement.executeQuery(queryString);
            String nodeString="<table style=\"width:100%\"> ";
            
            //PARSE THE RESULTS SET 
            while(rs.next()){
                //Retrieve DATA by column name
                double rev  = rs.getDouble("Rev");

                //BUILD NODE STRING FOR JSP
                 nodeString = nodeString + 
                                "  <tr> " +
                                "    <td> " + stockIdParam + "</td> " +
                                "    <td> " + rev + "</td> " +
                                "  </tr> ";
                
            }
            nodeString+="</table>";
            String storageKey = "StockRev";
            request.setAttribute(storageKey, nodeString);
            
            
            //SEND REQUEST TO JSP 
          String url ="./SalesByStockJSP.jsp" ;
          RequestDispatcher dispatcher = request.getRequestDispatcher(url);
          dispatcher.forward(request,response);   
            
            System.out.println("nodeString " + nodeString);
            //CLEAN UP CONNECTION
            rs.close();
            statement.close();
            connection.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (ClassNotFoundException ex) {
           Logger.getLogger(RevenueGeneratedByStockServlet.class.getName()).log(Level.SEVERE, null, ex);
       }finally{
            //finally block used to close resources
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
      
        }
               
    }
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    public String getServletInfo() {
        return "description";
    }
    
}
