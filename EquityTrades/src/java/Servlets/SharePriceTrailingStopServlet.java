package Servlets;

import Beans.StockSearchBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static sun.util.logging.LoggingSupport.log;

@WebServlet (name="SharePriceTrailingStopServlet",urlPatterns={"/SharePriceTrailingStopServlet"}) 
public class SharePriceTrailingStopServlet extends HttpServlet {

   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
       throws ServletException, IOException {

       String OrderIdParam = request.getParameter("OrderIdParam");
       // CONNECT AND SQL QUERIES 
        Connection connection = null;
        Statement statement = null;
        try{
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,EquityTradesJDBC.user,EquityTradesJDBC.dbPassword);
            statement = connection.createStatement();
            System.out.println("OrderIdParam is "+OrderIdParam);
            String queryString = "SELECT O.id AS id, S.PricePerShare AS PricePerShare, O.Percentage AS Percentage, "
                    + "S.StockSymbol AS StockSymbol FROM Orders O, Trade T, Stock S WHERE T.OrderId = O.Id AND " + 
            " O.Id = " + OrderIdParam + " AND T.StockId = S.StockSymbol;";
            ResultSet rs = statement.executeQuery(queryString);
            
            String nodeString="<table style=\"width:100%\"><tr><td>Order ID</td><td>Stock Symbol</td><td>Percentage</td></tr>";
            boolean notTrailingStop = false;
//PARSE THE RESULTS SET 
            while(rs.next()){
                //Retrieve DATA by column name
                int orderid = rs.getInt("id");
                double percentage  = rs.getDouble("Percentage");
                String stockSymbol  = rs.getString("StockSymbol");
                if (percentage == 0.0){
                    notTrailingStop = true;
                    break;
                }

                //BUILD NODE STRING FOR JSP
                 nodeString = nodeString + 
                                "  <tr> " +
                                "    <td>" + orderid + " </td> " +
                                "    <td>" + stockSymbol + " </td> " +
                                "    <td>" + percentage + "%"+ " </td> " +
                                "  </tr> ";
                
            }
            nodeString += "</table>";
            if(nodeString.equals("<table style=\"width:100%\"><tr><td>Order ID</td><td>Stock Symbol</td><td>Percentage</td></tr></table>") || notTrailingStop)
                nodeString = "<p>No trailing stop found.</p>";
            request.setAttribute("sharePriceTrailing", nodeString);
            
            //SEND REQUEST TO JSP 
            String url ="./C_SharePriceTrailingStopJSP.jsp" ;
            RequestDispatcher dispatcher = request.getRequestDispatcher(url);
            dispatcher.forward(request,response);   
            
            //CLEAN UP CONNECTION
            rs.close();
            statement.close();
            connection.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (ClassNotFoundException ex) {
           Logger.getLogger(SharePriceTrailingStopServlet.class.getName()).log(Level.SEVERE, null, ex);
       }finally{
            //finally block used to close resources
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
      
        }

    }
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    public String getServletInfo() {
        return "description";
    }
    
}
