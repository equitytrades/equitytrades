package Servlets;

import Beans.ClientBean;
import Beans.ClientList;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Wilson
 */
@WebServlet (name="DeleteClientServlet",urlPatterns={"/DeleteClientServlet"})
public class DeleteClientServlet extends HttpServlet {
    static String databaseURL = "jdbc:postgresql://130.245.72.54:5432/cse305";
    static String user = "postgres";
    static String dbPassword = "";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            Connection connection = null;
            Statement statement = null;
            try{
            //STEP 3: Open a connection
                String id = request.getParameter("client");
                id = id.replaceAll("\\s+","");
                String accID = "";
                System.out.println("getparameter id = " + id);
                System.out.println("Connecting to database...");
                Class.forName("org.postgresql.Driver");
                connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,user,dbPassword);

            //STEP 4: Execute a query
                System.out.println("Creating statement...");
                statement = connection.createStatement();
                String sql = "SELECT id FROM account WHERE account.clientid =" + id + ";";
                ResultSet rs = statement.executeQuery(sql);
                if(rs.next())
                    accID = rs.getString("id");
                sql = "SELECT accountid FROM trade WHERE accountid =" + accID + ";";
                rs = statement.executeQuery(sql);
                if(rs.next()) {
                    sql = "DELETE FROM trade WHERE accountid ="+ accID + ";";
                    statement.executeUpdate(sql);
                }
                sql = "DELETE FROM account WHERE id =" + accID + ";";
                statement.executeUpdate(sql);
                sql = "DELETE FROM client WHERE id =" + id + ";";
                statement.executeUpdate(sql);
                sql = "DELETE FROM login WHERE id = " + id + ";";
                statement.executeUpdate(sql);
                sql = "DELETE FROM person WHERE ssn ="+ id + ";";
                statement.executeUpdate(sql);
                sql = "DELETE FROM person WHERE ssn ="+ id + ";";
                statement.executeUpdate(sql);
                ClientList clientlist = (ClientList)request.getSession().getAttribute("cli");
                ClientBean clientToRemove = new ClientBean();
                int intId = Integer.parseInt(id);
                System.out.println("intId = " + intId);
                for (int i = 0; i < clientlist.getClientList().size(); i++){
                    if (intId == clientlist.getClientList().get(i).getClientId())
                        clientToRemove = clientlist.getClientList().get(i);
                }
                clientlist.removeClient(clientToRemove);
                String url = "./DeleteClientSuccessJSP.jsp";
                if (request.getSession().getAttribute("user").equals("representative"))
                    url = "./RDeleteClientSuccessJSP.jsp";
                RequestDispatcher dispatcher = request.getRequestDispatcher(url);
                dispatcher.forward(request, response);
            //STEP 6: Clean-up environment
            statement.close();
            connection.close();
            }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
            }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
            }finally{
            //finally block used to close resources
                try{
                    if(statement!=null)
                        statement.close();
                }catch(SQLException se2){
                }// nothing we can do
                try{
                    if(connection!=null)
                        connection.close();
                }catch(SQLException se){
                    se.printStackTrace();
                }//end finally try
            }//end try
            System.out.println("Goodbye!");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
