package Servlets;

import Beans.StockSearchBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static sun.util.logging.LoggingSupport.log;

@WebServlet (name="MostActivelyTradedStocksServlet",urlPatterns={"/MostActivelyTradedStocksServlet"}) 
public class MostActivelyTradedStocksServlet extends HttpServlet {

   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
       throws ServletException, IOException {
         
       //GET SESSION AND BEAN DATA
       HttpSession sess= request.getSession(true);

       //GET BEAN
       StockSearchBean stockBean = (StockSearchBean) sess.getAttribute("stockBean");
       if(stockBean==null){
          stockBean = new StockSearchBean();
          sess.setAttribute("stockBean",stockBean);
       }
       
       // SET BEAN DATA
       stockBean.setCompanyName(request.getParameter("searchStockBar"));
       
       // CONNECT AND SQL QUERIES 
        Connection connection = null;
        Statement statement = null;
        try{
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,EquityTradesJDBC.user,EquityTradesJDBC.dbPassword);
            statement = connection.createStatement();
            String queryString = "SELECT StockId FROM Trade GROUP BY StockId ORDER BY Count(*) DESC LIMIT 10";
                    //"SELECT T1.StockId AS StockName FROM Trade T1 GROUP BY StockId "
                    //+ "HAVING COUNT(*) >= ALL( SELECT COUNT(*) FROM Trade T2 GROUP BY T2.StockId )";
            ResultSet rs = statement.executeQuery(queryString);
            
            String nodeString="<table style=\"width:100%\"> ";
            //PARSE THE RESULTS SET 
            while(rs.next()){
                //Retrieve DATA by column name
                String stockName  = rs.getString("StockId");

                //BUILD NODE STRING FOR JSP
                 nodeString = nodeString + 
                                "  <tr> " +
                                "    <td>" + stockName + " </td> " +
                                "  </tr> ";
                
            }
            nodeString += "</table>";
            request.setAttribute("mostActivelyTradedStocks", nodeString);
            
            //SEND REQUEST TO JSP 
            String url ="./MostActiveStocksJSP.jsp" ;
            RequestDispatcher dispatcher = request.getRequestDispatcher(url);
            dispatcher.forward(request,response);          
        
            //CLEAN UP CONNECTION
            rs.close();
            statement.close();
            connection.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (ClassNotFoundException ex) {
           Logger.getLogger(MostActivelyTradedStocksServlet.class.getName()).log(Level.SEVERE, null, ex);
       }finally{
            //finally block used to close resources
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
      
        }
        
        
    }
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    public String getServletInfo() {
        return "description";
    }
    
}
