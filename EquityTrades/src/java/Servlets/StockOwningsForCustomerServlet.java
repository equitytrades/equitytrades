package Servlets;

import Beans.StockSearchBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static sun.util.logging.LoggingSupport.log;

@WebServlet (name="StockOwningsForCustomerServlet",urlPatterns={"/StockOwningsForCustomerServlet"}) 
public class StockOwningsForCustomerServlet extends HttpServlet {

   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
       throws ServletException, IOException {
       
       int clientId = (int) request.getSession().getAttribute("clientid");
       
        // CONNECT AND SQL QUERIES 
        Connection connection = null;
        Statement statement = null;
        try{
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,EquityTradesJDBC.user,EquityTradesJDBC.dbPassword);
            statement = connection.createStatement();
            
                    
            String queryString = "SELECT StockSymbol, NumShares FROM HasStock HS, Account A " + 
                    " WHERE A.clientid ="+ clientId +" AND HS.accountid= A.id ;";
            ResultSet rs = statement.executeQuery(queryString);
            
            String nodeString="<table style=\"width:100%\"><tr><td>Stock Symbol</td><td>Number of Shares</td></tr>";
            //PARSE THE RESULTS SET 
            while(rs.next()){
                //Retrieve DATA by column name
                String stockSymbol  = rs.getString("StockSymbol");
                int numShares  = rs.getInt("NumShares");

                //BUILD NODE STRING FOR JSP
                 nodeString = nodeString + 
                                "  <tr> " +
                                "    <td>" + stockSymbol + " </td> " +
                                "    <td>" + numShares + " </td> " +
                                "  </tr> ";
                
            }
            nodeString += "</table>";
            if(nodeString.equals("<table style=\"width:100%\"><tr><td>Stock Symbol</td><td>Number of Shares</td></tr></table>"))
                nodeString="<p>No Stocks For this User </p>";
            request.setAttribute("stockOwningsForCustomer", nodeString);
            
            //SEND REQUEST TO JSP 
            String url ="./C_StockOwningsForCustomerJSP.jsp" ;
            RequestDispatcher dispatcher = request.getRequestDispatcher(url);
            dispatcher.forward(request,response);   
          
            //CLEAN UP CONNECTION
            rs.close();
            statement.close();
            connection.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (ClassNotFoundException ex) {
           Logger.getLogger(StockOwningsForCustomerServlet.class.getName()).log(Level.SEVERE, null, ex);
       }finally{
            //finally block used to close resources
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
      
        }
        
    }
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    public String getServletInfo() {
        return "description";
    }
    
}
