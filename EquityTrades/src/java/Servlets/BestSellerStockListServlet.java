package Servlets;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet (name="BestSellerStockListServlet",urlPatterns={"/BestSellerStockListServlet"}) 
public class BestSellerStockListServlet extends HttpServlet {
   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
       throws ServletException, IOException {
        
       // CONNECT AND SQL QUERIES 
        Connection connection = null;
        Statement statement = null;
        try{
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,EquityTradesJDBC.user,EquityTradesJDBC.dbPassword);

            statement = connection.createStatement();
            String queryString = "SELECT StockId "+
                                    "FROM Trade "+
                                    "GROUP BY StockId "+
                                    "ORDER BY COUNT(*) DESC limit 10";
            
            ResultSet rs = statement.executeQuery(queryString);
            
            String nodeString= "<table style=\"width:100%\"> ";
            //PARSE THE RESULTS SET 
            while(rs.next()){
                //Retrieve DATA by column name
                String stockSymbol  = rs.getString("StockId");

              //Retrieve DATA by column name
                    nodeString = nodeString + 
                                "  <tr>         " +
                                "    <td>             " + stockSymbol + "       </td> " +
                                "  </tr> ";
            }
            nodeString += "</table>";
            if (nodeString.equals("<table style=\"width:100%\"></table>")){
                nodeString = "Unable to produce list of stock suggestions for given customer.";
            } 
            request.setAttribute("bestSellerStockList", nodeString);
            String url = "./BestSellerStockListJsp.jsp";
            RequestDispatcher dispatcher = request.getRequestDispatcher(url);
            dispatcher.forward(request, response);
           
            //CLEAN UP CONNECTION
            rs.close();
            statement.close();
            connection.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (ClassNotFoundException ex) {
           Logger.getLogger(BestSellerStockListServlet.class.getName()).log(Level.SEVERE, null, ex);
       }finally{
            //finally block used to close resources
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
      
        }
        
        //SEND REQUEST TO JSP 
        String url ="./BestSellerStockListJsp.jsp" ;
        RequestDispatcher dispatcher = request.getRequestDispatcher(url);
        dispatcher.forward(request,response);          

    }
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    public String getServletInfo() {
        return "description";
    }

    
}
