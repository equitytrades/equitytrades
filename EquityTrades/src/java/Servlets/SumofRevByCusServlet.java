/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import static Servlets.SetStockPriceServlet.databaseURL;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author KhaledAshraf
 */
@WebServlet(name = "SumofRevByCusServlet", urlPatterns = {"/SumofRevByCusServlet"})
public class SumofRevByCusServlet extends HttpServlet {
    static String databaseURL = "jdbc:postgresql://localhost:5432/cse305";
    static String user = "postgres";
    static String dbPassword = "";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            Connection connection = null;
            Statement statement = null;
            try{
                //STEP 3: Open a connection
                String first = request.getParameter("ClientFirst");
                String last = request.getParameter("ClientLast");
                System.out.println("Connecting to database...");
                Class.forName("org.postgresql.Driver");
                connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,user,dbPassword);
                
                //STEP 4: Execute a query
                System.out.println("Creating statement...");
                statement = connection.createStatement();
                String sql = "SELECT SUM(R.Fee) as revenue FROM Transactions R, Trade T where T.TransactionId = R.Id"
                                + " AND T.AccountId IN(SELECT A.Id FROM Account A WHERE A.ClientId IN("
                                    + "SELECT  C.id FROM Client C, Person P WHERE C.Id = P.SSN"
                                        + " AND P.LastName = '" + last + "' AND P.FirstName = '" + first + "'));";
                ResultSet rs = statement.executeQuery(sql);
                String url = "./failrevcus.jsp";
                if(rs.next() == true) {
                    request.setAttribute("clientFirst", first);
                    request.setAttribute("clientLast", last);
                    if(rs.getString("revenue") != null) {
                        request.setAttribute("sum", rs.getString("revenue"));
                        url =  "./sumrevcus.jsp";
                    }
                }
                RequestDispatcher dispatcher = request.getRequestDispatcher(url);
                dispatcher.forward(request, response);
                rs.close();
                statement.close();
                connection.close();
                
                
                
            }catch(Exception s) {
                s.printStackTrace();
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
