/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import static Servlets.MostRevByCusServlet.databaseURL;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author KhaledAshraf
 */
@WebServlet(name = "ListByStockSymbolServlet", urlPatterns = {"/ListByStockSymbolServlet"})
public class ListByStockSymbolServlet extends HttpServlet {
    static String databaseURL = "jdbc:postgresql://localhost:5432/cse305";
    static String user = "postgres";
    static String dbPassword = "";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String client = "";
            Connection connection = null;
            Statement statement = null;
            try{
                String sb = request.getParameter("stocksymbol1");
                //STEP 3: Open a connection
                System.out.println("Connecting to database...");
                Class.forName("org.postgresql.Driver");
                connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,user,dbPassword);
                
                //STEP 4: Execute a query
                System.out.println("Creating statement...");
                statement = connection.createStatement();
                String sql = "SELECT T.OrderId, T.AccountId FROM Trade T WHERE T.StockId = '" + sb + "';";
                ResultSet rs = statement.executeQuery(sql);
                String url = "./noOrder.jsp";
                request.setAttribute("stocksymbol1", sb);
                String sb1 =  "<table style=\"width: 100%\"><tr><td>Order ID</td><td>Account ID</td></tr>";
                int count = 0;
                while(rs.next()) {
                    count++;
                    sb1 += "<tr>";
                    sb1 += "<td>" + rs.getString("orderid") + "</td>";
                    sb1 += "<td>" + rs.getString("accountid") + "</td>";
                    sb1 += "</tr>";
                }
                sb1 += "</table>";
                if(count > 0) {
                    request.setAttribute("stocksymbol1",sb1);
                    url =  "./Orderlist.jsp";
                }
                
                RequestDispatcher dispatcher = request.getRequestDispatcher(url);
                dispatcher.forward(request, response);
                rs.close();
                statement.close();
                connection.close();
                
                
                
            }catch(Exception s) {
                s.printStackTrace();
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
