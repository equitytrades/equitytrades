/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Beans.EmployeeBean;
import Beans.EmployeeList;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Wilson
 */
@WebServlet (name="DeleteEmployeeServlet",urlPatterns={"/DeleteEmployeeServlet"})
public class DeleteEmployeeServlet extends HttpServlet {
    static String databaseURL = "jdbc:postgresql://130.245.72.54:5432/cse305";
    static String user = "postgres";
    static String dbPassword = "";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            Connection connection = null;
            Statement statement = null;
            try{
            //STEP 3: Open a connection
                String empId = request.getParameter("employee");
                empId = empId.replaceAll("\\s+","");
                int intId = Integer.parseInt(empId);
                System.out.println("Connecting to database...");
                Class.forName("org.postgresql.Driver");
                connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,user,dbPassword);

            //STEP 4: Execute a query
                System.out.println("Creating statement...");
                statement = connection.createStatement();
                String sql = "SELECT ssn FROM employee where id =" + empId + ";";
                ResultSet rs = statement.executeQuery(sql);
                int id = 0;
                while (rs.next()){
                    id = rs.getInt("ssn");
                }
                sql = "DELETE FROM employee WHERE ssn =" + id + ";";
                EmployeeList employeelist = (EmployeeList)request.getSession().getAttribute("emp");
                EmployeeBean employeeToRemove = new EmployeeBean();
                for (int i = 0; i < employeelist.getEmployeeList().size(); i++){
                    if (intId == employeelist.getEmployeeList().get(i).getEmployeeId())
                        employeeToRemove = employeelist.getEmployeeList().get(i);
                }
                employeelist.removeEmployee(employeeToRemove);
                statement.executeUpdate(sql);
                
                sql = "DELETE FROM login WHERE id = " + id + ";";
                statement.executeUpdate(sql);
                sql = "DELETE FROM person WHERE ssn ="+ id + ";";
                statement.executeUpdate(sql);
                String url = "./DeleteEmployeeSuccess.jsp";
                RequestDispatcher dispatcher = request.getRequestDispatcher(url);
                dispatcher.forward(request, response);
            //STEP 6: Clean-up environment
            statement.close();
            connection.close();
            }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
            }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
            }finally{
            //finally block used to close resources
                try{
                    if(statement!=null)
                        statement.close();
                }catch(SQLException se2){
                }// nothing we can do
                try{
                    if(connection!=null)
                        connection.close();
                }catch(SQLException se){
                    se.printStackTrace();
                }//end finally try
            }//end try
            System.out.println("Goodbye!");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
