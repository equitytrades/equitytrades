/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;
import Beans.ClientBean;
import Beans.ClientList;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Wilson
 */
@WebServlet (name="AddClientServlet",urlPatterns={"/AddClientServlet"})
public class AddClientServlet extends HttpServlet {


    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Connection connection = null;
        Statement statement = null;
        String url = "./AddClientFailJSP.jsp";
        if (request.getSession().getAttribute("user").equals("representative"))
            url = "./RAddClientFailJSP.jsp";
        String username = request.getParameter("username");//notnull
        String password = request.getParameter("password");//notnull
        String ssn = request.getParameter("ssn");//notnull\
        String firstname = request.getParameter("firstname");//notnull
        String lastname = request.getParameter("lastname");//notnull
        String address = request.getParameter("address");
        String zipcode = request.getParameter("zipcode");
        String telephone = request.getParameter("telephone");
        String email = request.getParameter("email"); //2015-11-11
        String rating = request.getParameter("rating");//notnull
        String credit = request.getParameter("credit");
        String year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        String month = String.valueOf(Calendar.getInstance().get(Calendar.MONTH));
        String day = String.valueOf(Calendar.getInstance().get(Calendar.DATE));
        String date = year + "-" + month + "-" + day;
        String accID = ssn + "1";
         
        if (!username.equals("") && !password.equals("") && !ssn.equals("") && !firstname.equals("")
            && !lastname.equals("")){
            System.out.println("inside if statement");
            if (address.equals(""))
                address = "NULL";
            else 
                address = "'" + address + "'";
            if (zipcode.equals(""))
                zipcode = "NULL";
            if (telephone.equals(""))
                telephone = "NULL";
            if (email.equals(""))
                email = "NULL";
            else 
                email = "'" + email + "'";
            if (rating.equals(""))
                rating = "NULL";
            if (credit.equals(""))
                credit = "NULL";
            
            try{
                System.out.println("Connecting to database...");
                Class.forName("org.postgresql.Driver");
                connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,EquityTradesJDBC.user,EquityTradesJDBC.dbPassword);

            //STEP 4: Execute a query
                System.out.println("Creating statement...");
                statement = connection.createStatement();
                String sql = "INSERT INTO person VALUES (" + ssn + ",'" + lastname
                                + "', '" + firstname + "', " + address 
                                + ", " + zipcode + ", " + telephone + ");";
                statement.executeUpdate(sql);
                sql = "INSERT INTO login VALUES (" + ssn + ", '" + username 
                                + "', '" + password + "');";
                statement.executeUpdate(sql);
                sql = "INSERT INTO client VALUES (" + email + ", "
                                + rating + ", " + credit + ", " + ssn+ ");"; 
                statement.executeUpdate(sql);
                sql = "INSERT INTO account VALUES (" + accID + ", '"
                                + date + "', " + ssn + ");"; 
                statement.executeUpdate(sql);
                sql = "SELECT * FROM client, person WHERE person.ssn = " + ssn + " AND client.id = person.ssn;";
                ResultSet rs = statement.executeQuery(sql);
                if (rs.next()){
                    ClientList clientList = (ClientList)request.getSession().getAttribute("cli");
                    ClientBean client = new ClientBean();
                    client.setClientId(rs.getInt("id"));
                    client.setFirstname(rs.getString("firstname"));
                    client.setLastname(rs.getString("lastname"));
                    client.setAddress(rs.getString("address"));
                    client.setZipcode(rs.getInt("zipcode"));
                    client.setTelephone(rs.getLong("telephone"));
                    client.setEmail(rs.getString("email"));
                    client.setRating(rs.getInt("rating"));
                    client.setCreditcardnumber(rs.getLong("creditcardnumber"));
                    clientList.addClient(client);
                }
                url = "./AddClientSuccessJSP.jsp";
                if (request.getSession().getAttribute("user").equals("representative"))
                    url = "./RAddClientSuccessJSP.jsp";
            //STEP 6: Clean-up environment
                rs.close();
                statement.close();
                connection.close();
            }catch(SQLException se){
            //Handle errors for JDBC
                se.printStackTrace();
            }catch(Exception e){
            //Handle errors for Class.forName
                e.printStackTrace();
            }finally{
            //finally block used to close resources
                try{
                    if(statement!=null)
                        statement.close();
                }catch(SQLException se2){
                }// nothing we can do
                try{
                    if(connection!=null)
                       connection.close();
                }catch(SQLException se){
                    se.printStackTrace();
                }//end finally try
            }//end try
            System.out.println("Goodbye!");
        }//end 
        RequestDispatcher dispatcher = request.getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}