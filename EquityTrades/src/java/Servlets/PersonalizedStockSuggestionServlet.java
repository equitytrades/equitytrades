import Servlets.EquityTradesJDBC;
import Beans.StockSearchBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static sun.util.logging.LoggingSupport.log;

@WebServlet (name="PersonalizedStockSuggestionServlet",urlPatterns={"/PersonalizedStockSuggestionServlet"}) 
public class PersonalizedStockSuggestionServlet extends HttpServlet {
    static String databaseURL = "jdbc:postgresql://130.245.72.94:5432/cse305";
    static String user = "postgres";
    static String dbPassword = "";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Connection connection = null;
        Statement statement = null;
        
        try{
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,EquityTradesJDBC.user,EquityTradesJDBC.dbPassword);
            statement = connection.createStatement();
            String nodeString="<p> Error: No Suggestions For this customer created yet </p>";
            String queryString = "SELECT * FROM StockSuggestionsForClient" + request.getSession().getAttribute("clientid");
            ResultSet rs = statement.executeQuery(queryString);
            nodeString= "<table style=\"width:100%\"> ";
            //PARSE THE RESULTS SET 
            while(rs.next()){
                //Retrieve DATA by column name
                String stockSymbol  = rs.getString("symbol");
                String companyName  = rs.getString("company");
                //BUILD NODE STRING FOR JSP
                 nodeString = nodeString + 
                                "  <tr> " +
                                "    <td>" + stockSymbol + " </td> " +
                                "    <td>" + companyName + " </td> " +
                                "  </tr> ";
            }
            nodeString += "</table>";
            request.setAttribute("stockSuggestionsResult", nodeString);
            
                rs.close();
                statement.close();
                connection.close();
                
            }catch(SQLException se){
            //Handle errors for JDBC
                se.printStackTrace();
            }catch(Exception e){
            //Handle errors for Class.forName
                e.printStackTrace();
            }finally{
            //finally block used to close resources
                try{
                    if(statement!=null)
                        statement.close();
                }catch(SQLException se2){
                }// nothing we can do
                try{
                    if(connection!=null)
                       connection.close();
                }catch(SQLException se){
                    se.printStackTrace();
                }//end finally try
            }//end try
            RequestDispatcher dispatcher = request.getRequestDispatcher("./C_PersonalizedStockSuggestionsResultJSP.jsp");
            dispatcher.forward(request, response);
        }//end 
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
