package Servlets;

import Beans.StockSearchBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static sun.util.logging.LoggingSupport.log;

@WebServlet (name="ShowAllStocksServlet",urlPatterns={"/ShowAllStocksServlet"}) 
public class ShowAllStocksServlet extends HttpServlet {

   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
       throws ServletException, IOException {
         

       // CONNECT AND SQL QUERIES 
        Connection connection = null;
        Statement statement = null;
        try{
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,EquityTradesJDBC.user,EquityTradesJDBC.dbPassword);
            statement = connection.createStatement();
            String queryString = "SELECT * FROM Stock;";
            ResultSet rs = statement.executeQuery(queryString);
            String nodeString=  "<table style=\"width:100%\"><tr><td>Stock Symbol</td><td>Company Name</td><td>Type</td><td>Price Per Share</td></tr>";
                               
            //PARSE THE RESULTS SET 
            while(rs.next()){
                //Retrieve DATA by column name
                String stockSymbol  = rs.getString("stocksymbol");
                String companyName  = rs.getString("companyname");
                String type  = rs.getString("type");
                double pricePerShare  = rs.getDouble("pricepershare");

                //BUILD NODE STRING FOR JSP
                 nodeString = nodeString + 
                                "  <tr> " +
                                "    <td>" + stockSymbol + " </td> " +
                                "    <td> " + companyName + "</td> " +
                                "    <td> " + type + " </td> " +
                                "    <td> " + pricePerShare + " </td> " +
                                "  </tr> ";
                         
            }
            nodeString += "</table>";
            if (nodeString.equals("<table style=\"width:100%\"><tr><td>Stock Symbol</td><td>Company Name</td><td>Type</td><td>Price Per Share</td></tr></table>"))
                nodeString = "There are no stocks in the database.";
            request.setAttribute("showAllStocks", nodeString);
            System.out.println("nodeString " + nodeString);
            
            //SEND REQUEST TO JSP 
            String url ="/ShowAllStocksJSP.jsp" ;
            RequestDispatcher dispatcher = request.getRequestDispatcher(url);
            dispatcher.forward(request,response);          
          
            //CLEAN UP CONNECTION
            rs.close();
            statement.close();
            connection.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (ClassNotFoundException ex) {
           Logger.getLogger(ShowAllStocksServlet.class.getName()).log(Level.SEVERE, null, ex);
       }finally{
            //finally block used to close resources
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
      
        }
        
        
    }
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    public String getServletInfo() {
        return "description";
    }
    
}
