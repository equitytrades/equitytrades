/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import static Servlets.MostRevByRepServlet.databaseURL;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author KhaledAshraf
 */
@WebServlet(name = "MostRevByCusServlet", urlPatterns = {"/MostRevByCusServlet"})
public class MostRevByCusServlet extends HttpServlet {
    static String databaseURL = "jdbc:postgresql://localhost:5432/cse305";
    static String user = "postgres";
    static String dbPassword = "";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String client = "";
            Connection connection = null;
            Statement statement = null;
            try{
                //STEP 3: Open a connection
                System.out.println("Connecting to database...");
                Class.forName("org.postgresql.Driver");
                connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,user,dbPassword);
                
                //STEP 4: Execute a query
                System.out.println("Creating statement...");
                statement = connection.createStatement();
                String sql = "SELECT ClientId FROM Account where Account.Id IN ( "
                        + "SELECT MAX(AccountId) as accountId FROM Trade WHERE AccountId IN(SELECT AccountId FROM Trade T INNER JOIN"
                        + " Transactions R ON T.TransactionId = R.Id "
                        + "GROUP BY AccountId "
                        + "ORDER BY SUM(Fee) DESC));";
                ResultSet rs = statement.executeQuery(sql);
                if(rs.next()) {
                    client = rs.getString("clientid");
                    sql = "SELECT firstname, lastname from Person WHERE SSN = "
                            + client + ";";
                    rs = statement.executeQuery(sql);
                }
                String url = "./noclients.jsp";
                if(rs.next() == true) {
                    request.setAttribute("firstname", rs.getString("firstname"));
                    request.setAttribute("lastname", rs.getString("lastname"));
                    request.setAttribute("accountId", client);
                    url =  "./MostActiveClient.jsp";
                }
                System.out.print(url);
                RequestDispatcher dispatcher = request.getRequestDispatcher(url);
                dispatcher.forward(request, response);
                rs.close();
                statement.close();
                connection.close();
                
                
                
            }catch(Exception s) {
                s.printStackTrace();
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
