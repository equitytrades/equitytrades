package Servlets;

import Servlets.EquityTradesJDBC;
import Beans.StockSearchBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static sun.util.logging.LoggingSupport.log;

@WebServlet (name="PastAndCurrentOrdersCustomerServlet",urlPatterns={"/PastAndCurrentOrdersCustomerServlet"}) 
public class PastAndCurrentOrdersCustomerServlet extends HttpServlet {

   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
       throws ServletException, IOException {
         
       int clientId = (int) request.getSession().getAttribute("clientid");
       
       // CONNECT AND SQL QUERIES 
        Connection connection = null;
        Statement statement = null;
        try{
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,EquityTradesJDBC.user,EquityTradesJDBC.dbPassword);
            statement = connection.createStatement();
            System.out.println("Client ID is " + clientId);
            String queryString = "SELECT O.Id AS OrderId , T.StockId As StockId, O.numshares As Shares FROM Trade T, Orders O Where T.OrderId = " + 
                    " O.Id AND T.AccountId IN (SELECT A.Id AS AccountID FROM Client C, Account A " +  
            " WHERE C.Id = " + clientId + " AND  C.Id = A.clientid);";
            ResultSet rs = statement.executeQuery(queryString);
            
            String nodeString="<table style=\"width:100%\"><tr><td>Order ID</td><td>Stock Symbol</td><td>Number of Shares</td></tr>";
            //PARSE THE RESULTS SET 
            while(rs.next()){
                 //Retrieve DATA by column name
                int OrderId  = rs.getInt("OrderId");
                String StockId  = rs.getString("StockId");
                int Shares  = rs.getInt("Shares");


                //BUILD NODE STRING FOR JSP
                 nodeString = nodeString + 
                                "  <tr> " +
                                "    <td>" + OrderId + " </td> " +
                                "    <td>" + StockId + " </td> " +
                                "    <td>" + Shares + " </td> " +
                                "  </tr> ";
                
            }
            nodeString += "</table>";
            if (nodeString.equals("<table style=\"width:100%\"><tr><td>Order ID</td><td>Stock Symbol</td><td>Number of Shares</td></tr></table>"))
                nodeString = "<p> No past or current orders.</p>";
            request.setAttribute("pastAndCurrentOrdersCustomer", nodeString);
            
            //SEND REQUEST TO JSP 
            String url ="./PastAndCurrentOrdersCustomerJSP.jsp" ;
            RequestDispatcher dispatcher = request.getRequestDispatcher(url);
            dispatcher.forward(request,response);   
            
            //CLEAN UP CONNECTION
            rs.close();
            statement.close();
            connection.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (ClassNotFoundException ex) {
           Logger.getLogger(PastAndCurrentOrdersCustomerServlet.class.getName()).log(Level.SEVERE, null, ex);
       }finally{
            //finally block used to close resources
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
      
        }
        
    }
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    public String getServletInfo() {
        return "description";
    }
    
}
