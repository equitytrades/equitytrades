package Servlets;

import Beans.StockSearchBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static sun.util.logging.LoggingSupport.log;

@WebServlet (name="SelectMaxSellingEmployeeServlet",urlPatterns={"/SelectMaxSellingEmployeeServlet"}) 
public class SelectMaxSellingEmployeeServlet extends HttpServlet {

   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
       throws ServletException, IOException {
         
       //GET SESSION AND BEAN DATA
       HttpSession sess= request.getSession(true);

       //GET BEAN
       StockSearchBean stockBean = (StockSearchBean) sess.getAttribute("stockBean");
       if(stockBean==null){
          stockBean = new StockSearchBean();
          sess.setAttribute("stockBean",stockBean);
       }
       
       // SET BEAN DATA
       stockBean.setCompanyName(request.getParameter("searchStockBar"));
       
       // CONNECT AND SQL QUERIES 
        Connection connection = null;
        Statement statement = null;
        try{
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,EquityTradesJDBC.user,EquityTradesJDBC.dbPassword);
            statement = connection.createStatement();
            String queryString = 
              "SELECT MAX(BrokerId) FROM Trade WHERE BrokerId IN ( SELECT BrokerId FROM Trade T INNER JOIN Transactions R ON T.TransactionId = R.Id" + 
              " GROUP BY BrokerId ORDER BY SUM(Fee) DESC);";
            
            ResultSet rs = statement.executeQuery(queryString);
            
            String nodeString="";
            //PARSE THE RESULTS SET 
            while(rs.next()){
                //Retrieve DATA by column name
                String stockSymbol  = rs.getString("stocksymbol");
                String companyName  = rs.getString("companyname");
                String type  = rs.getString("type");
                double pricePerShare  = rs.getDouble("pricepershare");

                //BUILD NODE STRING FOR JSP
                nodeString = nodeString+ "<p>stockSymbol: " + stockSymbol 
                        + " companyName: " + companyName+" type: " 
                        + type+" pricePerShare: " + pricePerShare + "</p> </br>";
                
            }
            stockBean.setNodeString(nodeString);
            System.out.println("nodeString " + nodeString);
            //CLEAN UP CONNECTION
            rs.close();
            statement.close();
            connection.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (ClassNotFoundException ex) {
           Logger.getLogger(SelectMaxSellingEmployeeServlet.class.getName()).log(Level.SEVERE, null, ex);
       }finally{
            //finally block used to close resources
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
      
        }
        
        //SEND REQUEST TO JSP 
        if(stockBean.getCompanyName()!=null && !stockBean.getCompanyName().equals("")){
          String url ="/WEB-INF/HandleStockSearchJsp.jsp" ;
          RequestDispatcher dispatcher = request.getRequestDispatcher(url);
          dispatcher.forward(request,response);          
        }   
    }
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    public String getServletInfo() {
        return "description";
    }
    
}
