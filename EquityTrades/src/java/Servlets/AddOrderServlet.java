/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import static Servlets.SetStockPriceServlet.databaseURL;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.exit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author KhaledAshraf
 */
@WebServlet(name = "AddOrderServlet", urlPatterns = {"/AddOrderServlet"})
public class AddOrderServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            Connection connection = null;
            Statement statement = null;
            try{
                //STEP 3: Open a connection
                String numShares = request.getParameter("numofshares") + "";
                Double pricePerShare = 0.0;
                String date = request.getParameter("date");
                String percentage = request.getParameter("percentage");
                String orderType = request.getParameter("ordertype");
                String transFee = request.getParameter("fee");
                String clientID = request.getParameter("clientID");
                String accID = "";
                String sb = request.getParameter("stocksym");
                String price = request.getParameter("price");
                String hiddenprice = request.getParameter("hiddenprice");
                int brokerid = (int)request.getSession().getAttribute("brokerid");
                int orderID = 0;
                int transID = 0;
                System.out.println("Connecting to database...");
                Class.forName("org.postgresql.Driver");
                connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,EquityTradesJDBC.user,EquityTradesJDBC.dbPassword);
                
                //STEP 4: Execute a query
                System.out.println("Creating statement...");
                statement = connection.createStatement();
                
                //GET ACCOUNT ID
                String sql = "SELECT id from Account WHERE Account.clientid = " + clientID + ";";
                ResultSet rs = statement.executeQuery(sql);
                while(rs.next())
                    accID = rs.getString("id");
                
                sql = "SELECT numshares from hasstock where accountid = " + accID + " AND stocksymbol = '" + sb + "';";
                rs = statement.executeQuery(sql);
                boolean flag = false;
                while(rs.next()) {
                    if(orderType.equals("Buy")) {
                        numShares = String.valueOf(Integer.parseInt(numShares) + Integer.parseInt(rs.getString("numshares")));
                        
                        flag = true;
                    } else if(orderType.equals("Sell") && (Integer.parseInt(rs.getString("numshares")) - Integer.parseInt(numShares) >= 0)) {
                        numShares = String.valueOf(Integer.parseInt(rs.getString("numshares")) - Integer.parseInt(numShares));
                        flag = true;
                    }
                }
                
                //GET MOST RECENT OrderID
                sql = "SELECT COUNT(id) as id FROM Orders;";
                
                rs = statement.executeQuery(sql);
                if(rs.next())
                    orderID = (int)rs.getInt("id") + 1;

                //GET MOST RECENT TransactionsID
                sql = "SELECT COUNT(id) as id FROM Transactions;";
                rs = statement.executeQuery(sql);
                if(rs.next())
                    transID = (int)rs.getInt("id") + 1;

                if(numShares.equals("")) {
                    numShares = "1";
                }
                if(date.equals("")) {
                    date = "1969-01-01";
                }
                if(percentage.equals("")) {
                    percentage = "0";
                }
                if(orderType.equals("")) {
                    orderType = "NULL";
                }
                if(transFee.equals("")) {
                    transFee = "0";
                }
                if(clientID.equals("")) {
                    clientID = "0";
                }
                if(sb.equals("")) {
                    sb = "NULL";
                }
                if(price.equals("")) {
                    price = "Market";
                }
                if(hiddenprice.equals("")) {
                    hiddenprice = "NULL";
                }
                //GET PRICE PER SHARE FROM STOCK TABLE
                sql = "SELECT PricePerShare FROM Stock WHERE StockSymbol = '" + sb + "';";
                rs = statement.executeQuery(sql);
                if(rs.next()) {
                    pricePerShare = rs.getDouble("pricepershare");
                }

                //INSERT INTO ORDERS
                if(flag || Integer.parseInt(numShares) > 0) {
                    sql = "INSERT INTO Orders VALUES(" + numShares + ", " + pricePerShare + ", " + orderID + ", '" + date +
                            "', " + percentage + ", '"  + price + "', '" + orderType + "', " + hiddenprice + ");";
                    statement.executeUpdate(sql);
                }
               
               	//INSERT INTO TRANSACTIONS
                if(flag || Integer.parseInt(numShares) > 0 ) {
                    sql = "INSERT INTO Transactions VALUES(" + transID + ", " + transFee + ", '" + date + "', " + pricePerShare + ");";
                    statement.executeUpdate(sql);
                }

                //INSERT INTO TRADE
                if(flag || Integer.parseInt(numShares) > 0) {
                    sql = "INSERT INTO TRADE VALUES(" + accID + ", " + brokerid +  ", " + transID + ", " + orderID + ", '" + sb + "');";
                    statement.executeUpdate(sql);
                }
                
                if(flag == true) {
                    sql = "UPDATE hasstock SET numshares = " + numShares + " where accountid = " + accID + " AND stocksymbol = '" + sb + "';";
                    statement.executeUpdate(sql);
                    String url =  "./insertorder.jsp";
                    RequestDispatcher dispatcher = request.getRequestDispatcher(url);
                    dispatcher.forward(request, response);
                    rs.close();
                    statement.close();
                    connection.close();
                }
                        
                //INSERT INTO HASSTOCK
                if(Integer.parseInt(numShares) > 0 && !flag) {
                    sql = "INSERT INTO HASSTOCK VALUES(" + accID + ", '" + sb +  "', " + numShares + ");";
                    statement.executeUpdate(sql);
                    String url =  "./insertorder.jsp";
                    RequestDispatcher dispatcher = request.getRequestDispatcher(url);
                    dispatcher.forward(request, response);
                    rs.close();
                    statement.close();
                    connection.close();
                }
            }catch(SQLException s) {
                s.printStackTrace();
                String url = "./failinsertorder.jsp";
                RequestDispatcher dispatcher = request.getRequestDispatcher(url);
                dispatcher.forward(request, response);
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
