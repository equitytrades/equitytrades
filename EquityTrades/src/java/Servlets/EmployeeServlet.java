/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import static Servlets.MailingListServlet.user;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Wilson
 */
@WebServlet(name = "EmployeeServlet", urlPatterns = {"/EmployeeServlet"})
public class EmployeeServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Connection connection = null;
        Statement statement = null;
        int brokerId = (int)request.getSession().getAttribute("brokerid");
            try{
                System.out.println("Connecting to database...");
                Class.forName("org.postgresql.Driver");
                connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,EquityTradesJDBC.user,EquityTradesJDBC.dbPassword);
                String nodeString=  "<table style=\"width:100%\"><tr><td>Name</td><td>Telephone</td><td>Address</td><td>Start Date</td></tr>";

            //STEP 4: Execute a query
                System.out.println("Creating statement...");
                statement = connection.createStatement();
                String sql = "SELECT firstname, lastname, telephone, address, startdate FROM employee, person WHERE id != " + brokerId + " and employee.ssn = person.ssn;";
                ResultSet rs = statement.executeQuery(sql);
                while (rs.next()){
                    String firstname = rs.getString("firstname");
                    String lastname = rs.getString("lastname");
                    String telephone = rs.getString("telephone");
                    String address = rs.getString("address");
                    String startdate = rs.getString("startdate");
                    if (telephone == null || telephone.equals("0")) telephone = "N/A";
                    if (address == null)address = "N/A";
                    if (startdate== null)startdate = "N/A";
                        
                    nodeString = nodeString + 
                                "  <tr> " +
                                "    <td>" + firstname + " " + lastname +"</td> " +
                                "    <td>" + telephone + " </td> " +
                                "    <td>" + address + " </td> " +
                                "    <td>" + startdate + " </td> " +
                                "  </tr> ";
                }
                nodeString += "</table>";
            //STEP 6: Clean-up environment
                if (nodeString.equals("<table style=\"width:100%\"><tr><td>Name</td><td>Telephone</td><td>Address</td><td>Start Date</td></tr></table>")){
                    nodeString = "<p>  There are no employees.</p>";
                } 
                request.setAttribute("employeelist", nodeString);
                String url = "./EmployeeTableJSP.jsp";
                RequestDispatcher dispatcher = request.getRequestDispatcher(url);
                dispatcher.forward(request, response);
                rs.close();
                statement.close();
                connection.close();
            }catch(SQLException se){
            //Handle errors for JDBC
                se.printStackTrace();
            }catch(Exception e){
            //Handle errors for Class.forName
                e.printStackTrace();
            }finally{
            //finally block used to close resources
                try{
                    if(statement!=null)
                        statement.close();
                }catch(SQLException se2){
                }// nothing we can do
                try{
                    if(connection!=null)
                       connection.close();
                }catch(SQLException se){
                    se.printStackTrace();
                }//end finally try
            }//end try
            System.out.println("Goodbye!");
        }//end 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}