/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Wilson
 */
@WebServlet(name = "SearchServlet", urlPatterns = {"/SearchServlet"})
public class SearchServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         
       String input = request.getParameter("input");
       
       // CONNECT AND SQL QUERIES 
        Connection connection = null;
        Statement statement = null;
        try{
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(EquityTradesJDBC.databaseURL,EquityTradesJDBC.user,EquityTradesJDBC.dbPassword);
            statement = connection.createStatement();
            String queryString = "SELECT * FROM Stock WHERE companyname LIKE '%" + input + "%' or type = '" + input + "';";
            ResultSet rs = statement.executeQuery(queryString);
            String nodeString=  "<table style=\"width:100%\"><tr><td>Symbol</td><td>Company Name</td><td>Type</td><td>Price per Share</td></tr>";
            //PARSE THE RESULTS SET 
            while(rs.next()){
                String symbol = rs.getString("stocksymbol");
                String companyname = rs.getString("companyname");
                Double pricepershare = rs.getDouble("pricepershare");
                String type = rs.getString("type");
                nodeString = nodeString + 
                                "<tr>" +
                                "<td>" + symbol + "</td>" +
                                "<td>" + companyname + "</td>" +
                                "<td>" + type + "</td>" +
                                "<td>" + pricepershare + "</td>" +
                                "</tr> ";
            }
            nodeString += "</table>";
            if (nodeString.equals("<table style=\"width:100%\"><tr><td>Symbol</td><td>Company Name</td><td>Type</td><td>Price per Share</td></tr></table>")){
                nodeString = "  No stocks found.";
            }
            request.setAttribute("search", nodeString);
            String url = "./RSearchJSP.jsp";
            if (request.getSession().getAttribute("user").equals("manager"))
                url = "./SearchJSP.jsp";
            else if (request.getSession().getAttribute("user").equals("client"))
                url = "./C_SearchJSP.jsp";
            RequestDispatcher dispatcher = request.getRequestDispatcher(url);
            dispatcher.forward(request, response);
            //CLEAN UP CONNECTION
            rs.close();
            statement.close();
            connection.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (ClassNotFoundException ex) {
       }finally{
            //finally block used to close resources
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }  
    }
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    public String getServletInfo() {
        return "description";
    }
    
}