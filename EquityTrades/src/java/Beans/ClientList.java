/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import java.util.ArrayList;

/**
 *
 * @author Wilson
 */
public class ClientList {
    private ArrayList<ClientBean> clientList = new ArrayList<ClientBean>();
    public ClientList(){}
    public ArrayList<ClientBean> getClientList(){
        return clientList;
    }
    public void addClient(ClientBean client){
        this.clientList.add(client);
    }
    public void removeClient(ClientBean client){
        this.clientList.remove(client);
    }
}
