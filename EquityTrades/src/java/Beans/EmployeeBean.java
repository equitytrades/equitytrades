/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import java.sql.Date;

/**
 *
 * @author Wilson
 */
public class EmployeeBean {
    private int employeeId;
    private int ssn;
    private String firstname;
    private String lastname;
    private String address;
    private int zipcode;
    private long telephone;
    private Date startdate;
    private int hourlyrate;
    public EmployeeBean(){}

    /**
     * @return the employeeId
     */
    public int getEmployeeId() {
        return employeeId;
    }

    /**
     * @param employeeId the employeeId to set
     */
    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    /**
     * @return the firstName
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstname(String firstName) {
        this.firstname = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastname(String lastName) {
        this.lastname = lastName;
    }
    
    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startDate) {
        this.startdate = startDate;
    }

    public int getHourlyrate() {
        return hourlyrate;
    }

    public void setHourlyrate(int hourlyrate) {
        this.hourlyrate = hourlyrate;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getZipcode() {
        return zipcode;
    }

    public void setZipcode(int zipcode) {
        this.zipcode = zipcode;
    }

    public long getTelephone() {
        return telephone;
    }

    public void setTelephone(long telephone) {
        this.telephone = telephone;
    }
    public int getSsn() {
        return ssn;
    }

    public void setSsn(int ssn) {
        this.ssn = ssn;
    }
}
