package Beans;

public class StockSearchBean {
    private String companyName;
    private String nodeString="_";
    
    public void setCompanyName(String name){
        companyName = name;
    }
    public String getCompanyName(){
        return companyName;
    }
    public void setNodeString(String str){
        nodeString = str;
    }
    public String getNodeString(){
        return nodeString;
    }
    
    
}
