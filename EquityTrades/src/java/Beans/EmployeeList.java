/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import java.util.ArrayList;

/**
 *
 * @author Wilson
 */
public class EmployeeList {
    private ArrayList<EmployeeBean> employeeList = new ArrayList<EmployeeBean>();
    public EmployeeList(){}
    public ArrayList<EmployeeBean> getEmployeeList(){
        return employeeList;
    }
    public void addEmployee(EmployeeBean employee){
        this.employeeList.add(employee);
    }
    public void removeEmployee(EmployeeBean employee){
        this.employeeList.remove(employee);
    }
}
