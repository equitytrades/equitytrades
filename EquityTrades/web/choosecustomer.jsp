<%-- 
    Document   : choosecustomer
    Created on : May 2, 2016, 3:24:39 AM
    Author     : KhaledAshraf
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Equity Trades Inc.</title>

    <!-- Bootstrap Core CSS -->
    <link href="./bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="./bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="./dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="./bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="./bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="./managerJSP.jsp">Equity Trades Inc.</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> ${sessionScope.username}
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li class="divider"></li>
                        <li><a href="http://localhost:8080/EquityTrades/LogoutServlet"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <form action="http://localhost:8080/EquityTrades/SearchServlet" class="input-group custom-search-form" method="post">
                                <input type="text" class="form-control" placeholder="Search Stocks..." name="input">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </form>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="#" class="active"><i class="fa fa-group"></i> Manage Users<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li>
                                    <a href="./AddEmployeeJSP.jsp">Add Employee</a>
                                </li>
                                <li>
                                    <a href="./EditEmployeeJSP.jsp">Edit Employee</a>
                                </li>
                                 <li>
                                    <a href="./DeleteEmployeeJSP.jsp">Delete Employee</a>
                                </li>
                                <li>
                                    <a href="./AddClientJSP.jsp">Add Client</a>
                                </li>
                                <li>
                                    <a href="./EditClientJSP.jsp">Edit Client</a>
                                </li>
                                <li>
                                    <a href="./DeleteClientJSP.jsp">Delete Client</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="http://localhost:8080/EquityTrades/ObtainMonthlySalesReportServlet" class="active"><i class="fa fa-book"></i> Sales Reports</a>                         
                        </li>
                        <li>
                            <a href="#" class="active"><i class="fa fa-dollar"></i> Revenues<span class="fa arrow"></span></a> 
                            <ul class="nav nav-second-level collapse"> 
                                <li>
                                <a href="#">Summary of Revenues<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse">
                                    <li class="second-lev">
                                        <a href="http://localhost:8080/EquityTrades/QueryRevenueByStockJSP.jsp">By Stock Name</a>
                                    </li>
                                    <li class="second-lev">
                                        <a href="http://localhost:8080/EquityTrades/QueryRevenueByStockTypeJSP.jsp">By Stock Type</a>
                                    </li>
                                    <li class="second-lev">
                                        <a href="./choosecustomer.jsp">By Customer</a>
                                    </li>
                                </ul>
                                </li>
                        </li>
                        <li>
                                <a href="#">Most Revenue Generated<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse">
                                    <li class="second-lev">
                                        <a href="http://localhost:8080/EquityTrades/MostRevByRepServlet">By Customer Representative</a>
                                    </li>
                                    <li class="second-lev">
                                        <a href="http://localhost:8080/EquityTrades/MostRevByCusServlet">By Customer</a>
                                    </li>
                                </ul>
                        </li>
                        </ul>
                         <li>
                            <a href="#" class="active"><i class="fa fa-list-alt"></i> List of Orders<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse">
                            <!-- /.nav-second-level -->
                                <li class="second-lev">
                                    <a href="./choosestocksymbol.jsp">By Stock Symbol</a>
                                </li>
                                <li class="second-lev">
                                    <a href="./listordersbycustomer.jsp">Customer Name</a>
                                </li>
                                </ul>
                        </li>
                        <li>
                            <a href="#" class="active"><i class="fa fa-database"></i> Stocks<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse">
                                    <li class=second-lev>
                                        <a href="./setshareprice.jsp">Set Price of Stock</a>
                                    </li>
                                    <li class ="second-lev">
                                        <a href="http://localhost:8080/EquityTrades/ShowAllStocksServlet">Show All Stocks</a>
                                    </li>
                                    <li class="second-lev">
                                        <a href="http://localhost:8080/EquityTrades/MostActivelyTradedStocksServlet">Hot Stocks</a>
                                    </li>
                                </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Clients</h1>
                </div>
                <div class="col-md-4 col-md-offset-4">
                    <div class=" panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Enter Client First & Last Name</h3>
                        </div>
                        <div class="panel-body">
                            <form action="http://localhost:8080/EquityTrades/SumofRevByCusServlet" method="POST" role="form">
                                <fieldset>
                                <div class="form-group">
                                        <input class="form-control" placeholder="First Name" name="ClientFirst" type="text" value="" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Last Name" name="ClientLast" type="text" value="">
                                    </div>
                                    <!-- Change this to a button or input when using this as a form -->
                                    <input type="submit" value="Done" class="btn btn-lg btn-success btn-block"/>
                                </fieldset>
                            </form>
                        </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="./bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="./bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="./bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="./bower_components/raphael/raphael-min.js"></script>
<!--    <script src="./bower_components/morrisjs/morris.min.js"></script>
    <script src="./js/morris-data.js"></script>-->

    <!-- Custom Theme JavaScript -->
    <script src="./dist/js/sb-admin-2.js"></script>

</body>

</html>

