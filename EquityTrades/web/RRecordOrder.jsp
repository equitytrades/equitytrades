<%-- 
    Document   : RRecordOrder
    Created on : May 2, 2016, 10:04:30 PM
    Author     : KhaledAshraf
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Equity Trades Inc.</title>

    <!-- Bootstrap Core CSS -->
    <link href="./bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="./bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="./dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="./bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="./bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
    div.add-form {
        padding-top: 0.2cm;
        padding-left: 6cm;
        padding-right: 6cm;
    }
    </style>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="./RepresentativeJSP.jsp">Equity Trades Inc.</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> ${sessionScope.username}
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        
                        <li><a href="http://localhost:8080/EquityTrades/LogoutServlet"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <form action="http://localhost:8080/EquityTrades/SelectStockByKeywordServlet" class="input-group custom-search-form" method="post">
                                <input type="text" class="form-control" placeholder="Search Orders..." name="input">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </form>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="#" class="active"><i class="fa fa-group"></i> Manage Clients<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li>
                                    <a href="./RAddClientJSP.jsp">Add Client</a>
                                </li>
                                <li>
                                    <a href="./REditClientJSP.jsp">Edit Client</a>
                                </li>
                                <li>
                                    <a href="./RDeleteClientJSP.jsp">Delete Client</a>
                                </li>
                                <li>
                                    <a href="http://localhost:8080/EquityTrades/MailingListServlet">Mailing List</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="RRecordOrder.jsp" class="active"><i class="fa fa-book"></i> Record an Order</a>                         
                        </li>
                        <li>
                            <a href="./RStockSuggestionJSP.jsp" class="active"><i class="fa fa-book"></i> Suggest Stocks</a>                         
                        </li>
                            <!-- /.nav-second-level -->
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Orders</h1>
                </div>
                </div>
                <div class="add-form">
                    <div class=" panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Record Order</h3>
                        </div>
                        <div class="panel-body">
                            <form action="http://localhost:8080/EquityTrades/AddOrderServlet" method="post" role="form">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Number Of Shares" name="numofshares" type="text" value="" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Date YYYY-MM-DD" name="date" type="text" value="">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Percentage" name="percentage" type="text" value="">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Price Type" name="price" type="text" value="">
                                    </div>
                                     <div class="form-group">
                                        <input class="form-control" placeholder="Order Type" name="ordertype" type="text" value="">
                                    </div>
                                     <div class="form-group">
                                        <input class="form-control" placeholder="Transaction Fee" name="fee" type="text" value="">
                                    </div>
                                    <div class="form-group">
                                        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
                                        <select name="clientID" class="form-control">
                                        <c:forEach items="${cli.clientList}" var="cl">
                                            <option value="${cl.clientId}">${cl.clientId} - ${cl.firstname} ${cl.lastname}</option>
                                        </c:forEach>
                                    </select>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Stock Symbol" name="stocksym" type="text" value="">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Hideen Price" name="hiddenprice" type="text" value="">
                                    </div>
                                    <!-- Change this to a button or input when using this as a form -->
                                    <input type="submit" value="Add Order" class="btn btn-lg btn-success btn-block"/>
                                </fieldset>
                            </form>
                        </div>
                </div>
            </div>
            </div>
        </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="./bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="./bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="./bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="./bower_components/raphael/raphael-min.js"></script>
    <!--<script src="./bower_components/morrisjs/morris.min.js"></script>-->
    <!--<script src="./js/morris-data.js"></script>-->

    <!-- Custom Theme JavaScript -->
    <script src="./dist/js/sb-admin-2.js"></script>

</body>

</html>
